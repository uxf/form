<?php

declare(strict_types=1);

namespace UXF\FormTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use UXF\FormTests\Entity\Article;
use UXF\FormTests\Entity\ArticleUniqInfo;
use UXF\FormTests\Entity\Status;
use UXF\FormTests\Entity\Tag;

class FormDataFixtures implements ORMFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $tag = new Tag('name');
        $manager->persist($tag);

        $tag2 = new Tag('name 2');
        $manager->persist($tag2);

        $tag3 = new Tag('name 3');
        $manager->persist($tag3);

        $tag4 = new Tag('xxx 4');
        $manager->persist($tag4);

        $article = new Article('', $tag, new Date('2020-01-01'), new DateTime('2020-02-02T10:00+01:00'), new Time('10:00'), Status::NEW);
        $manager->persist($article);

        $articleUniqInfo = new ArticleUniqInfo('uniq name');
        $manager->persist($articleUniqInfo);

        $manager->flush();
    }
}
