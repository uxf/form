<?php

declare(strict_types=1);

namespace UXF\FormTests\Func;

use UXF\Core\Test\WebTestCase;

class FormControllerTest extends WebTestCase
{
    public function testGetSchema(): void
    {
        $client = self::createClient();
        $client->get('/api/cms/form/article/schema');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'fields' => [
                [
                    'name' => 'integer',
                    'type' => 'integer',
                    'label' => 'integer',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'float',
                    'type' => 'float',
                    'label' => 'float',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'string',
                    'type' => 'string',
                    'label' => 'string',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'text',
                    'type' => 'text',
                    'label' => 'text',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'date',
                    'type' => 'date',
                    'label' => 'date',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'datetime',
                    'type' => 'datetime',
                    'label' => 'datetime',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'time',
                    'type' => 'time',
                    'label' => 'time',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'manyToOne',
                    'type' => 'manyToOne',
                    'label' => 'manyToOne',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'tag',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'manyToMany',
                    'type' => 'manyToMany',
                    'label' => 'manyToMany',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'tag',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'oneToMany',
                    'type' => 'oneToMany',
                    'label' => 'oneToMany',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'id',
                            'type' => 'integer',
                            'label' => 'id',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'text',
                            'type' => 'string',
                            'label' => 'text',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'oneToOne',
                    'type' => 'oneToOne',
                    'label' => 'oneToOne',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'article',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'oneToOneFieldset',
                    'type' => 'oneToOneFieldset',
                    'label' => 'oneToOneFieldset',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'integer',
                            'type' => 'integer',
                            'label' => 'integer',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'string',
                            'type' => 'string',
                            'label' => 'string',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'text',
                            'type' => 'text',
                            'label' => 'text',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'date',
                            'type' => 'date',
                            'label' => 'date',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'datetime',
                            'type' => 'datetime',
                            'label' => 'datetime',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'time',
                            'type' => 'time',
                            'label' => 'time',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'manyToOne',
                            'type' => 'manyToOne',
                            'label' => 'manyToOne',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => 'tag',
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'manyToMany',
                            'type' => 'manyToMany',
                            'label' => 'manyToMany',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => 'tag',
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'info',
                    'type' => 'embedded',
                    'label' => 'info',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'integer',
                            'type' => 'integer',
                            'label' => 'integer',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'float',
                            'type' => 'float',
                            'label' => 'float',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'string',
                            'type' => 'string',
                            'label' => 'string',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'text',
                            'type' => 'text',
                            'label' => 'text',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'date',
                            'type' => 'date',
                            'label' => 'date',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'datetime',
                            'type' => 'datetime',
                            'label' => 'datetime',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'time',
                            'type' => 'time',
                            'label' => 'time',
                            'required' => false,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'status',
                    'type' => 'enum',
                    'label' => 'status',
                    'required' => false,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => [
                        [
                            'id' => 'NEW',
                            'label' => 'NEW',
                            'color' => null,
                        ],
                        [
                            'id' => 'OLD',
                            'label' => 'OLD',
                            'color' => null,
                        ],
                    ],
                    'fields' => [],
                ],
            ],
        ], $client->getResponseData());
    }

    public function testGet(): void
    {
        $client = self::createClient();
        $client->get('/api/cms/form/article/1');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'integer' => 0,
            'float' => 0,
            'string' => '',
            'text' => '',
            'date' => '2020-01-01',
            'datetime' => '2020-02-02T10:00:00+01:00',
            'time' => '10:00:00',
            'manyToOne' => [
                'id' => 1,
                'label' => 'name',
            ],
            'manyToMany' => [],
            'oneToMany' => [],
            'oneToOne' => null,
            'oneToOneFieldset' => [
                'integer' => 0,
                'string' => '',
                'text' => '',
                'date' => '2020-01-01',
                'datetime' => '2020-02-02T10:00:00+01:00',
                'time' => '10:00:00',
                'manyToOne' => [
                    'id' => 1,
                    'label' => 'name',
                ],
                'manyToMany' => [],
            ],
            'info' => [
                'integer' => 1000,
                'float' => 1000,
                'string' => '100',
                'text' => '100!!!',
                'date' => '2030-01-01',
                'datetime' => '2030-02-02T10:00:00+01:00',
                'time' => '10:00:00',
            ],
            'status' => 'NEW',
        ], $client->getResponseData());
    }

    public function testCreate(): void
    {
        $body = [
            'integer' => 1,
            'float' => 2.2,
            'string' => 'string',
            'text' => 'text',
            'date' => '2020-01-01',
            'datetime' => '2020-02-02T10:00:00+01:00',
            'time' => '13:10:00',
            'manyToOne' => [
                'id' => 1,
                'label' => 'name',
            ],
            'manyToMany' => [[
                'id' => 1,
                'label' => '1 - name',
            ], [
                'id' => 2,
                'label' => '2 - name 2',
            ]],
            'oneToMany' => [[
                'text' => 'test comment',
            ]],
            'oneToOne' => [
                'id' => 1,
                'label' => 'uniq name',
            ],
            'oneToOneFieldset' => [
                'integer' => 555,
                'string' => '5555',
                'text' => '55555',
                'date' => '2021-01-01',
                'datetime' => '2021-02-02T10:00:00+01:00',
                'time' => '13:10:00',
                'manyToOne' => [
                    'id' => 2,
                    'label' => 'name 2',
                ],
                'manyToMany' => [[
                    'id' => 1,
                    'label' => 'name',
                ]],
            ],
            'info' => [
                'integer' => 666,
                'float' => 666.66,
                'string' => '666',
                'text' => '666!!!',
                'date' => '2031-01-01',
                'datetime' => '2031-02-02T10:00:00+01:00',
                'time' => '13:10:00',
            ],
            'status' => 'OLD',
        ];

        $client = self::createClient();
        $client->post('/api/cms/form/article', $body);
        self::assertResponseIsSuccessful();

        $body['oneToMany'] = [[
            'id' => 1,
            'text' => 'test comment',
        ]];
        self::assertSame($body, $client->getResponseData());
    }

    public function testUpdate(): void
    {
        $body = [
            'integer' => 10,
            'float' => 22.2,
            'string' => 'string new',
            'text' => 'text new',
            'date' => '2021-01-01',
            'datetime' => '2021-02-02T10:00:00+01:00',
            'time' => '11:00:59',
            'manyToOne' => [
                'id' => 2,
                'label' => 'name 2',
            ],
            'manyToMany' => [[
                'id' => 3,
                'label' => '3 - name 3',
            ]],
            'oneToMany' => [[
                'text' => 'test comment 2',
            ]],
            'oneToOne' => null,
            'oneToOneFieldset' => [
                'integer' => 1,
                'string' => '2',
                'text' => '3',
                'date' => '2020-01-01',
                'datetime' => '2020-02-02T10:00:00+01:00',
                'time' => '11:00:59',
                'manyToOne' => [
                    'id' => 2,
                    'label' => 'name 2',
                ],
                'manyToMany' => [[
                    'id' => 2,
                    'label' => 'name 2',
                ]],
            ],
            'info' => [
                'integer' => 888,
                'float' => 888.66,
                'string' => '888',
                'text' => '888!!!',
                'date' => '2032-01-01',
                'datetime' => '2032-02-02T10:00:00+01:00',
                'time' => '11:00:59',
            ],
            'status' => 'NEW',
        ];

        $client = self::createClient();
        $client->put('/api/cms/form/article/2', $body);
        self::assertResponseIsSuccessful();

        $body['oneToMany'] = [[
            'id' => 2,
            'text' => 'test comment 2',
        ]];
        self::assertSame($body, $client->getResponseData());
    }

    public function testRemove(): void
    {
        $client = self::createClient();
        $client->delete('/api/cms/form/article/1');
        self::assertResponseIsSuccessful();
        self::assertSame([], $client->getResponseData());
    }
}
