<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

enum Status: string
{
    case NEW = 'NEW';
    case OLD = 'OLD';
}
