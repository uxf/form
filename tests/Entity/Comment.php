<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Comment
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'oneToMany'), ORM\JoinColumn(nullable: false)]
    private Article $article;

    #[ORM\Column]
    private string $text;

    public function __construct(Article $parent, string $text)
    {
        $this->article = $parent;
        $this->text = $text;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
