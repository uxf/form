<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;

#[ORM\Entity]
class ArticleFieldsetInfo
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    private int $integer = 0;

    #[ORM\Column]
    private string $string = '';

    #[ORM\Column(type: 'text')]
    private string $text = '';

    #[ORM\Column(type: Date::class)]
    private Date $date;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $datetime;

    #[ORM\Column(type: Time::class)]
    private Time $time;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private Tag $manyToOne;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    private Collection $manyToMany;

    public function __construct(Tag $manyToOne)
    {
        $this->date = new Date('2020-01-01');
        $this->datetime = new DateTime('2020-02-02 10:00');
        $this->time = new Time('10:00');
        $this->manyToOne = $manyToOne;
        $this->manyToMany = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getInteger(): int
    {
        return $this->integer;
    }

    public function setInteger(int $integer): void
    {
        $this->integer = $integer;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): void
    {
        $this->string = $string;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date): void
    {
        $this->date = $date;
    }

    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function setTime(Time $time): void
    {
        $this->time = $time;
    }

    public function getManyToOne(): Tag
    {
        return $this->manyToOne;
    }

    public function setManyToOne(Tag $manyToOne): void
    {
        $this->manyToOne = $manyToOne;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getManyToMany(): Collection
    {
        return $this->manyToMany;
    }

    /**
     * @param Collection<int, Tag> $manyToMany
     */
    public function setManyToMany(Collection $manyToMany): void
    {
        $this->manyToMany = $manyToMany;
    }
}
