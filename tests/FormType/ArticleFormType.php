<?php

declare(strict_types=1);

namespace UXF\FormTests\FormType;

use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use UXF\Form\Field\BasicField;
use UXF\Form\Field\EmbeddedField;
use UXF\Form\Field\EnumField;
use UXF\Form\Field\ManyToManyField;
use UXF\Form\Field\ManyToOneField;
use UXF\Form\Field\OneToManyField;
use UXF\Form\Field\OneToOneField;
use UXF\Form\Field\OneToOneFieldsetField;
use UXF\Form\FormBuilder;
use UXF\Form\FormType;
use UXF\FormTests\Entity\Article;
use UXF\FormTests\Entity\ArticleFieldsetInfo;
use UXF\FormTests\Entity\ArticleUniqInfo;
use UXF\FormTests\Entity\Comment;
use UXF\FormTests\Entity\Info;
use UXF\FormTests\Entity\Status;
use UXF\FormTests\Entity\Tag;

/**
 * @implements FormType<Article>
 */
class ArticleFormType implements FormType
{
    public function buildForm(FormBuilder $builder): void
    {
        $builder->setClassName(Article::class);
        $builder->addField(new BasicField('integer', 'int', 'integer'));
        $builder->addField(new BasicField('float', 'float', 'float'));
        $builder->addField(new BasicField('string', 'string', 'string'));
        $builder->addField(new BasicField('text', 'string', 'text'));
        $builder->addField(new BasicField('date', Date::class, 'date'));
        $builder->addField(new BasicField('datetime', DateTime::class, 'datetime'));
        $builder->addField(new BasicField('time', Time::class, 'time'));
        $builder->addField(new ManyToOneField('manyToOne', Tag::class, 'name', 'tag'));
        $builder->addField(new ManyToManyField('manyToMany', Tag::class, 'name', 'tag'));
        $builder->addField(new OneToManyField('oneToMany', Comment::class, [
            new BasicField('id', 'int', 'integer'),
            new BasicField('text', 'string', 'string'),
        ]));

        $builder->addField(new OneToOneField('oneToOne', ArticleUniqInfo::class, 'name', 'article'));

        $builder->addField(new OneToOneFieldsetField('oneToOneFieldset', ArticleFieldsetInfo::class, [
            new BasicField('integer', 'int', 'integer'),
            new BasicField('string', 'string', 'string'),
            new BasicField('text', 'string', 'text'),
            new BasicField('date', Date::class, 'date'),
            new BasicField('datetime', DateTime::class, 'datetime'),
            new BasicField('time', Time::class, 'time'),
            new ManyToOneField('manyToOne', Tag::class, 'name', 'tag'),
            new ManyToManyField('manyToMany', Tag::class, 'name', 'tag'),
        ]));

        $builder->addField(new EmbeddedField('info', Info::class, [
            new BasicField('integer', 'int', 'integer'),
            new BasicField('float', 'float', 'float'),
            new BasicField('string', 'string', 'string'),
            new BasicField('text', 'string', 'text'),
            new BasicField('date', Date::class, 'date'),
            new BasicField('datetime', DateTime::class, 'datetime'),
            new BasicField('time', Time::class, 'time'),
        ]));

        $builder->addField(new EnumField('status', Status::class));

        $manyToManyField = $builder->getField('manyToMany');
        if ($manyToManyField instanceof ManyToManyField) {
            $manyToManyField->setLabelCallback(fn (Tag $tag) => "{$tag->getId()} - {$tag->getName()}");
        }
    }

    public static function getName(): string
    {
        return 'article';
    }
}
