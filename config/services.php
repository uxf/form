<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Form\Controller\FormController;
use UXF\Form\FormBuilderFactory;
use UXF\Form\FormFactory;
use UXF\Form\Mapper\BasicMapper;
use UXF\Form\Mapper\EmbeddedMapper;
use UXF\Form\Mapper\EnumMapper;
use UXF\Form\Mapper\ManyToManyMapper;
use UXF\Form\Mapper\ManyToOneMapper;
use UXF\Form\Mapper\OneToManyMapper;
use UXF\Form\Service\EntityInstantiator;
use UXF\Form\Service\FormEntityManager;
use UXF\Form\Service\MapperResolver;
use UXF\Form\SimpleFormBuilderFactory;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire();

    $services->alias(ContainerInterface::class, 'service_container');

    $services->set(FormFactory::class);
    $services->set(EntityInstantiator::class);
    $services->set(FormEntityManager::class);
    $services->set(MapperResolver::class);

    $services->set(SimpleFormBuilderFactory::class);
    $services->alias(FormBuilderFactory::class, SimpleFormBuilderFactory::class);

    $services->set(FormController::class)
        ->public();

    $services->set(BasicMapper::SERVICE_ID, BasicMapper::class)->public();
    $services->set(EmbeddedMapper::SERVICE_ID, EmbeddedMapper::class)->public();
    $services->set(EnumMapper::SERVICE_ID, EnumMapper::class)->public();
    $services->set(ManyToManyMapper::SERVICE_ID, ManyToManyMapper::class)->public();
    $services->set(ManyToOneMapper::SERVICE_ID, ManyToOneMapper::class)->public();
    $services->set(OneToManyMapper::SERVICE_ID, OneToManyMapper::class)->public();
};
