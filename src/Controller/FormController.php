<?php

declare(strict_types=1);

namespace UXF\Form\Controller;

use Nette\Utils\Json;
use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;
use UXF\Core\Contract\Autocomplete\AutocompleteResult;
use UXF\Form\FormFactory;
use UXF\Form\Http\FormAutocompleteRequestQuery;
use UXF\Form\Schema\FormSchema;
use UXF\Form\Service\FormEntityManager;

final readonly class FormController
{
    public function __construct(
        private FormEntityManager $formEntityManager,
        private FormFactory $formFactory,
        private ?AutocompleteFactory $autocompleteFactory = null,
    ) {
    }

    public function getSchema(string $name): FormSchema
    {
        return $this->formFactory->createSchema($name);
    }

    /**
     * @return AutocompleteResult[]
     */
    public function autocomplete(string $name, #[FromQuery] FormAutocompleteRequestQuery $query): array
    {
        return $this->autocompleteFactory?->createAutocomplete($name)->search($query->term, 20) ?? [];
    }

    /**
     * @return array<string, mixed>
     */
    public function get(string $name, int $id): array
    {
        return $this->formEntityManager->loadFormValues($name, $id);
    }

    /**
     * @return array<string, mixed>
     */
    public function create(Request $request, string $name): array
    {
        $data = Json::decode($request->getContent(), forceArrays: true);
        return $this->formEntityManager->saveRow($name, $data);
    }

    /**
     * @return array<string, mixed>
     */
    public function update(Request $request, string $name, int $id): array
    {
        $data = Json::decode($request->getContent(), forceArrays: true);
        return $this->formEntityManager->saveRow($name, $data, $id);
    }

    public function remove(string $name, int $id): void
    {
        $this->formEntityManager->removeRow($name, $id);
    }
}
