<?php

declare(strict_types=1);

namespace UXF\Form\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use UXF\Core\Exception\BasicException;
use UXF\Form\Event\EntityEditedEvent;
use UXF\Form\FormFactory;

final readonly class FormEntityManager
{
    public function __construct(
        private EntityManagerInterface $em,
        private FormFactory $formFactory,
        private EntityInstantiator $entityInstantiator,
        private MapperResolver $mapperResolver,
        private EventDispatcherInterface $eventDispatcher,
    ) {
    }

    /**
     * @param mixed[] $data
     * @return mixed[]
     */
    public function saveRow(string $entityAlias, array $data, ?int $id = null): array
    {
        $form = $this->formFactory->createForm($entityAlias);
        /** @phpstan-var class-string<object> $className */
        $className = $form->className;
        $new = false;

        if ($id !== null) {
            $object = $this->em->getRepository($className)->find($id);
        } elseif (is_callable($form->instantiator)) {
            $object = call_user_func($form->instantiator, $data);
            $new = true;
        } else {
            $object = $this->entityInstantiator->instantiate($form, $data);
            $new = true;
        }

        if ($object === null) {
            throw BasicException::notFound();
        }

        $fields = $form->fields;

        foreach ($data as $fieldName => $value) {
            $field = $fields[$fieldName] ?? null;
            if ($field === null || $field->isReadOnly() || $field->isHidden()) {
                continue;
            }

            $this->mapperResolver->resolveMapper($field)->mapToEntity($field, $object, $value);
        }

        $this->em->persist($object);
        $this->em->flush();

        $this->eventDispatcher->dispatch(new EntityEditedEvent($object, $new));

        return $this->loadFormValues($entityAlias, $object->getId());
    }

    public function removeRow(string $entityAlias, int $id): void
    {
        /** @phpstan-var class-string<object> $className */
        $className = $this->formFactory->createForm($entityAlias)->className;
        $object = $this->em->find($className, $id);

        if ($object === null) {
            throw BasicException::notFound();
        }

        $this->em->remove($object);
        $this->em->flush();
    }

    /**
     * @return mixed[]
     */
    public function loadFormValues(string $entityAlias, int $id): array
    {
        $form = $this->formFactory->createForm($entityAlias);
        /** @phpstan-var class-string<object> $className */
        $className = $form->className;

        $object = $this->em->find($className, $id);

        if ($object === null) {
            throw BasicException::notFound();
        }

        $formValues = [];

        foreach ($form->fields as $field) {
            $value = $this->mapperResolver->resolveMapper($field)->mapFromEntity($field, $object);
            $formValues[$field->getName()] = $value;
        }

        return $formValues;
    }
}
