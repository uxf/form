<?php

declare(strict_types=1);

namespace UXF\Form\Service;

use Psr\Container\ContainerInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Mapper\Mapper;

final readonly class MapperResolver
{
    public function __construct(private ContainerInterface $container)
    {
    }

    public function resolveMapper(Field $field): Mapper
    {
        $mapper = $field->getMapper();
        if ($mapper !== null) {
            return $mapper;
        }

        $mapperServiceId = $field->getDefaultMapperServiceId();
        if (!$this->container->has($mapperServiceId)) {
            throw new FormException("Service $mapperServiceId doesn't exists");
        }

        $mapper = $this->container->get($mapperServiceId);
        if (!$mapper instanceof Mapper) {
            throw new FormException("Service $mapperServiceId isn't type of " . Mapper::class);
        }

        $field->setMapper($mapper);
        return $mapper;
    }
}
