<?php

declare(strict_types=1);

namespace UXF\Form;

/**
 * @template T of object
 */
interface FormType
{
    /**
     * @param FormBuilder<T> $builder
     */
    public function buildForm(FormBuilder $builder): void;

    public static function getName(): string;
}
