<?php

declare(strict_types=1);

namespace UXF\Form;

/**
 * @implements FormBuilderFactory<object>
 */
final readonly class SimpleFormBuilderFactory implements FormBuilderFactory
{
    /**
     * @return FormBuilder<object>
     */
    public function tryCreateBuilder(string $entityAlias): FormBuilder
    {
        return new FormBuilder();
    }
}
