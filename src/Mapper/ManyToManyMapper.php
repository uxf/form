<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Field\LabelField;

final readonly class ManyToManyMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.many_to_many_mapper';

    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        throw new FormException('Unsupported method');
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$field instanceof LabelField) {
            throw new FormException('Unsupported field');
        }

        if ($this->propertyAccessor->isWritable($entity, $field->getName())) {
            $ids = array_map(static fn (array $v) => $v['id'], $value);

            /** @var class-string<object> $targetClassName - phpstan hint */
            $targetClassName = $field->getTargetClassName();
            $items = $this->entityManager->getRepository($targetClassName)->findBy([
                'id' => $ids,
            ]);
            $this->propertyAccessor->setValue($entity, $field->getName(), new ArrayCollection($items));
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if (!$field instanceof LabelField) {
            throw new FormException('Unsupported field');
        }

        $items = [];

        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            $labelCallback = $field->getLabelCallback();
            foreach ($this->propertyAccessor->getValue($entity, $field->getName()) as $item) {
                $items[] = [
                    'id' => $this->propertyAccessor->getValue($item, 'id'),
                    'label' => is_callable($labelCallback) ?
                        $labelCallback($item) :
                        $this->propertyAccessor->getValue($item, $field->getTargetField()),
                ];
            }
        }

        return $items;
    }
}
