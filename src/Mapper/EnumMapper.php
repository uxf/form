<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Core\Exception\ValidationException;
use UXF\Form\Field\Field;

final readonly class EnumMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.enum_mapper';

    public function __construct(private PropertyAccessorInterface $propertyAccessor)
    {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        return self::convert($field->getPhpType(), $value, $field->getName());
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if ($this->propertyAccessor->isWritable($entity, $field->getName())) {
            $value = self::convert($field->getPhpType(), $value, $field->getName());
            $this->propertyAccessor->setValue($entity, $field->getName(), $value);
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            return $this->propertyAccessor->getValue($entity, $field->getName());
        }

        return null;
    }

    private static function convert(string $type, mixed $value, string $name): mixed
    {
        if ($value === null) {
            return null;
        }

        return $type::tryFrom($value) ?? throw new ValidationException([[
            'field' => $name,
            'message' => "Invalid enum value '$value'",
        ]]);
    }
}
