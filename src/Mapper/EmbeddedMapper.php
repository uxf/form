<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Field\Fieldset;
use UXF\Form\Service\MapperResolver;

final readonly class EmbeddedMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.embedded_mapper';

    public function __construct(
        private MapperResolver $mapperResolver,
        private PropertyAccessorInterface $propertyAccessor,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        throw new FormException('Unsupported method');
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$field instanceof Fieldset) {
            throw new FormException('Unsupported field');
        }

        $object = $this->propertyAccessor->getValue($entity, $field->getName());

        foreach ($value as $fieldName => $v) {
            $innerField = $field->getFields()[$fieldName] ?? null;
            if ($innerField === null || $innerField->isReadOnly() || $innerField->isHidden()) {
                continue;
            }

            $this->mapperResolver->resolveMapper($innerField)->mapToEntity($innerField, $object, $v);
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if (!$field instanceof Fieldset) {
            throw new FormException('Unsupported field');
        }

        $object = $this->propertyAccessor->getValue($entity, $field->getName());

        $result = [];
        foreach ($field->getFields() as $innerField) {
            $value = $this->mapperResolver->resolveMapper($innerField)->mapFromEntity($innerField, $object);
            $result[$innerField->getName()] = $value;
        }
        return $result;
    }
}
