<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use UXF\Form\Field\Field;

interface Mapper
{
    public function mapToEntityConstructor(Field $field, mixed $value): mixed;

    public function mapToEntity(Field $field, object $entity, mixed $value): void;

    public function mapFromEntity(Field $field, object $entity): mixed;
}
