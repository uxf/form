<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Exception;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Form\Field\BasicField;
use UXF\Form\Field\Field;

final readonly class BasicMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.basic_mapper';

    public function __construct(private PropertyAccessorInterface $propertyAccessor)
    {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        return self::convert($field->getPhpType(), $value, $field->getName());
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        assert($field instanceof BasicField);

        if ($this->propertyAccessor->isWritable($entity, $field->getName())) {
            $value = self::convert($field->getPhpType(), $value, $field->getName());
            $this->propertyAccessor->setValue($entity, $field->getName(), $value);
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            return $this->propertyAccessor->getValue($entity, $field->getName());
        }

        return null;
    }

    private static function convert(string $type, mixed $value, string $name): mixed
    {
        if ($value === null) {
            return null;
        }

        try {
            return match ($type) {
                'int' => (int) $value,
                'float' => (float) $value,
                Date::class => new Date($value),
                DateTime::class => new DateTime($value),
                Decimal::class => Decimal::of($value),
                Url::class => Url::of($value),
                Time::class => new Time($value),
                Email::class => Email::of($value),
                Phone::class => Phone::of($value),
                BankAccountNumberCze::class => BankAccountNumberCze::of($value),
                NationalIdentificationNumberCze::class => NationalIdentificationNumberCze::of($value),
                default => $value,
            };
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => $name,
                'message' => $e->getMessage(),
            ]]);
        }
    }
}
