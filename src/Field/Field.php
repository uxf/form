<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\Mapper;

abstract class Field
{
    private string $name;
    private string $phpType;
    private string $type;
    private string $label;
    private bool $required = false;
    private bool $readOnly = false;
    private bool $editable = true;
    private bool $hidden = false;
    private int $order = 10;
    private ?Mapper $mapper = null;

    public function __construct(string $name, string $phpType, string $type, ?string $label = null)
    {
        $this->name = $name;
        $this->label = $label ?? $name;
        $this->phpType = $phpType;
        $this->type = $type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPhpType(): string
    {
        return $this->phpType;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): void
    {
        $this->required = $required;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): void
    {
        $this->readOnly = $readOnly;
    }

    public function isEditable(): bool
    {
        return $this->editable;
    }

    public function setEditable(bool $editable): void
    {
        $this->editable = $editable;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    public function getMapper(): ?Mapper
    {
        return $this->mapper;
    }

    public function setMapper(?Mapper $mapper): void
    {
        $this->mapper = $mapper;
    }

    abstract public function getDefaultMapperServiceId(): string;
}
