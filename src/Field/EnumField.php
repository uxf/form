<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use BackedEnum;
use UXF\Form\Mapper\EnumMapper;
use UXF\Form\Schema\OptionSchema;

final class EnumField extends Field
{
    /**
     * @var OptionSchema[]
     */
    private array $options;

    /**
     * @param OptionSchema[]|null $options
     */
    public function __construct(string $name, string $phpType, ?array $options = null)
    {
        assert(is_a($phpType, BackedEnum::class, true));

        parent::__construct($name, $phpType, 'enum');

        if ($options === null) {
            $options = [];
            foreach ($phpType::cases() as $item) {
                $options[] = new OptionSchema($item->value, $item->name, null);
            }
        }

        $this->options = $options;
    }

    public function getDefaultMapperServiceId(): string
    {
        return EnumMapper::SERVICE_ID;
    }

    /**
     * @return OptionSchema[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param OptionSchema[] $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }
}
