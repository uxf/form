<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\ManyToOneMapper;

/**
 * @implements LabelField<mixed>
 */
final class OneToOneField extends Field implements LabelField
{
    use LabelFieldTrait;

    /**
     * @phpstan-param class-string $phpType
     */
    public function __construct(string $name, string $phpType, string $targetField, string $autocomplete)
    {
        parent::__construct($name, $phpType, 'oneToOne');
        $this->targetClassName = $phpType;
        $this->targetField = $targetField;
        $this->autocomplete = $autocomplete;
    }

    public function getDefaultMapperServiceId(): string
    {
        return ManyToOneMapper::SERVICE_ID;
    }
}
