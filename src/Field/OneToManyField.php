<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\OneToManyMapper;

final class OneToManyField extends Field implements Fieldset
{
    /** @phpstan-var class-string */
    private string $targetClassName;

    /** @var array<string, Field> */
    private array $fields = [];

    /**
     * @phpstan-param class-string $phpType
     * @param Field[] $fields
     */
    public function __construct(string $name, string $phpType, array $fields)
    {
        parent::__construct($name, $phpType, 'oneToMany');
        $this->targetClassName = $phpType;
        foreach ($fields as $field) {
            $this->fields[$field->getName()] = $field;
        }
    }

    /**
     * @phpstan-return class-string
     */
    public function getTargetClassName(): string
    {
        return $this->targetClassName;
    }

    /**
     * @return array<string, Field>
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritDoc
     */
    public function getClassName(): string
    {
        return $this->targetClassName;
    }

    /**
     * @param array<string, Field> $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    public function getDefaultMapperServiceId(): string
    {
        return OneToManyMapper::SERVICE_ID;
    }
}
