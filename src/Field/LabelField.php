<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use Closure;

/**
 * @phpstan-template T
 */
interface LabelField
{
    /**
     * @phpstan-return class-string<T>
     */
    public function getTargetClassName(): string;

    public function getTargetField(): string;

    public function getAutocomplete(): string;

    /**
     * @phpstan-return Closure(T): string
     */
    public function getLabelCallback(): ?Closure;
}
