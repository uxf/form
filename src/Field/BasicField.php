<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\BasicMapper;

final class BasicField extends Field
{
    public function getDefaultMapperServiceId(): string
    {
        return BasicMapper::SERVICE_ID;
    }
}
