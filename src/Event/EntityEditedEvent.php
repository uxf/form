<?php

declare(strict_types=1);

namespace UXF\Form\Event;

use Symfony\Contracts\EventDispatcher\Event;

final class EntityEditedEvent extends Event
{
    public function __construct(public readonly mixed $entity, public readonly bool $new)
    {
    }
}
