<?php

declare(strict_types=1);

namespace UXF\Form\Schema;

final readonly class OptionSchema
{
    public function __construct(
        public string|int $id,
        public string $label,
        public ?string $color,
    ) {
    }
}
